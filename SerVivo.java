package cc.Capitulo21;

public class SerVivo {
	
	private String reino;
	private int idade;
	private String tipoRespiracao;
	private String tipoReproducao;
	
	public String getTipoReproducao() {
		return tipoReproducao;
	}
	public void setTipoReproducao(String tipoReproducao) {
		this.tipoReproducao = tipoReproducao;
	}
	public String getReino() {
		return reino;
	}
	public void setReino(String reino) {
		this.reino = reino;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getTipoRespiracao() {
		return tipoRespiracao;
	}
	public void setTipoRespiracao(String tipoRespiracao) {
		this.tipoRespiracao = tipoRespiracao;
	}
	
	public void respirar(){
		System.out.println("Respirando...");
	}
	public void reproduzir(){
		System.out.println("Reproduzindo...");
	}
	
}
