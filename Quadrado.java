package cc.Capitulo21;

public class Quadrado extends FigurasGeometricas{
	
	double lado;
	
	Quadrado(double lado){
		this.lado = lado;
	}
	
	double calculaAreaQuadrado(double lado){
		return lado * lado;
	}
}
