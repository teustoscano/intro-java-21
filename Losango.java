package cc.Capitulo21;

public class Losango extends FigurasGeometricas{

	double lado;
	double diagonalMaior;
	double diagonalMenor;
	
	Losango(double l, double dma, double dme){
		lado = l;
		diagonalMaior = dma;
		diagonalMenor = dme;
	}
	
	double calculaAreaLosango(double lado, double dma, double dme){
		return (dma * dme)/2;
	}
}
