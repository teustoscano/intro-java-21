package cc.Capitulo21;

public class Monera extends SerVivo{

	private String nome;
	private String tipo;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String respirar(){
		return super.respirar() + "Monera";
	}
	public String reproduzir(){
		return super.reproduzir() + "Monera";
	}
}
