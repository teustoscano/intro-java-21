package cc.Capitulo21;

public class Retangulo extends FigurasGeometricas{
	
	double ladoA;
	double ladoB;
	
	Retangulo(double a, double b){
		ladoA = a;
		ladoB = b;
	}
	
	double calculaAreaRetangulo(double ladoA, double ladoB){
		return ladoA * ladoB;
	}
}
