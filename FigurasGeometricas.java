package cc.Capitulo21;

public class FigurasGeometricas {

	private String nome;
	private int numerosLados;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getNumerosLados() {
		return numerosLados;
	}
	public void setNumerosLados(int numerosLados) {
		this.numerosLados = numerosLados;
	}
	
}
